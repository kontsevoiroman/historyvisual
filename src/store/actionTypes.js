export const ADD_EVENT = 'ADD_EVENT'
export const INIT_CHART = 'INIT_CHART'
export const RESIZE_CHART = 'RESIZE_CHART'
export const SORT_EVENTS = 'SORT_EVENTS'
