import { createStore } from 'redux'
import historyApp from './reducers'

const store = createStore(historyApp)

export default store