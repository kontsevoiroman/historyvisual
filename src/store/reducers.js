import {combineReducers} from 'redux'
import {
    SORT_EVENTS,
    INIT_CHART,
    RESIZE_CHART,
    ADD_EVENT,
} from './actionTypes'

import {EVENTS} from "../constants";

function eventsToDraw(state = EVENTS, action) {
    switch (action.type) {
        case SORT_EVENTS:
            const filteredEvents = action.events.map(el => {
                const newEl = {...el}
                delete newEl.country
                delete newEl.leftShift
                return newEl
            })
            localStorage.setItem('events', JSON.stringify(filteredEvents))
            return action.events
        case ADD_EVENT:
            return [
                ...state,
                action.event
            ]
        default:
            return state
    }
}

const initialChartState = {
    scaleWidth: 0,
    minYear: null,
    maxYear: null,
    maxShift: null,
    currentRealMaxShift: null,
    initialScale: 0,
    scale: 0,
    totalMoveToLeftSize: 0,
}

function chartState(state = initialChartState, {type, payload}) {
    switch (type) {
        case INIT_CHART:
            return {
                ...state,
                ...payload,
            }
        case RESIZE_CHART:
            console.log(payload)
            return {
                ...state,
                ...payload,
            }
        default:
            return state
    }
}

const historyApp = combineReducers({
    eventsToDraw,
    chartState,
})

export default historyApp