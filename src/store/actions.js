import {
    ADD_EVENT,
    INIT_CHART,
    RESIZE_CHART,
    SORT_EVENTS,
} from "./actionTypes";

/*
 * action creators
 */

export function setEventsToStore(events) {
    return {
        type: SORT_EVENTS,
        events,
    }
}

export function addEventToStore(event) {
    return {
        type: ADD_EVENT,
        event,
    }
}

export function initChart(payload) {
    return {type: INIT_CHART, payload}
}

export function resizeChart(payload) {
    return {type: RESIZE_CHART, payload}
}