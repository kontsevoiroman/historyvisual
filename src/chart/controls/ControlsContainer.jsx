import React from 'react'
import styled from "styled-components";

const ControlsWrapper = styled.div`
  width: 100%;
`

class ControlsContainer extends React.Component {

    render() {
      const {decreaseOnce, increaseOnce} = this.props
        return (
            <ControlsWrapper ref={this.chartRef}>
              <button
                onClick={increaseOnce}
              >Увеличить</button>
              <button
                onClick={decreaseOnce}
              >Уменьшить</button>
            </ControlsWrapper>
        )
    }
}

export default ControlsContainer