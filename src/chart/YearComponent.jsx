import React from 'react'
import styled from 'styled-components'

const YearLabel = styled.div`
  text-align: center;
  height: 18px;
  position: absolute;
  left: 0;
  transform: translateX(-50%);
  top: 100%;
  font-size: 14px;
`

const setTransformIfNearBorder = (isFirst, isLast) => {
    if (isFirst) {
        return 'translate(0)'
    } else if (isLast) {
        return 'translate(-100%)'
    }
    return null
}

const YearComponent = (
    {
        year,
        leftShift,
        maxShift,
        totalMoveToLeftSize,
    }
) => {

    const isFirst = leftShift + totalMoveToLeftSize < 40
    const isLast = maxShift - leftShift - totalMoveToLeftSize < 40

    const transform = setTransformIfNearBorder(isFirst, isLast)

    return (
        <YearLabel
            style={{
                transform
            }}
        >{year}</YearLabel>
    )
}

export default YearComponent