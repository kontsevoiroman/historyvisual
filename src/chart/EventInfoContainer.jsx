import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.div`
  width: 200px;
  display: block;
  height: 48px;
  position: absolute;
  top: 0;
  left: 1px;
  overflow-y: auto;
  background: #f0f0f0;
  z-index: 2;
  box-shadow: 0 0 1px 0 #454545;
  &:hover {
    z-index: 3;
    box-shadow: 0 0 2px 1px #454545;
  }
`

const Title = styled.div`
  width: 100%;
  padding: 0 8px;
  font-size: 10px;
  font-weight: 700;
`

const Period = styled.div`
  width: 100%;
  padding: 4px 8px 0;
  font-size: 9px;
  font-weight: 300;
  color: #3e4c7d;
`

const EventInfoContainer = ({
    title,
    dateInfo,
    leftShift,
    maxShift,
    totalMoveToLeftSize,
}) => {

    const isLast = maxShift - leftShift - totalMoveToLeftSize < 200

    return (
        <Wrapper
            style={{
                left: isLast ? 'auto' : null,
                right: isLast ? '1px' : null
            }}
        >
            <Title>{title}</Title>
            {
                dateInfo &&
                <Period>({dateInfo})</Period>
            }
        </Wrapper>
    )
}


export default EventInfoContainer