import React from 'react'
import styled from 'styled-components'
import {connect} from 'react-redux'

import EventInfoContainer from './EventInfoContainer'
import ControlsContainer from './controls/ControlsContainer'

import {EVENTS, INCREMENT} from '../constants'
import YearComponent from "./YearComponent";
import {
    initChart,
    setEventsToStore,
    resizeChart,
} from '../store/actions'

const ChartWrapper = styled.div`
  width: 100%;
  height: 360px;
  position: relative;
  border: 1px solid black;
`

const InnerWrapper = styled.div`
  position: absolute;
  top: 0;
  bottom: 0;
  left: 20px;
  right: 19px;
  overflow: hidden;
`

const ZoomedContainer = styled.div`
  position: absolute;
  top: 0;
  bottom: 0;
`

const Scale = styled.div`
  position: absolute;
  bottom: 30px;
  height: 20px;
  left: 0;
  right: 1px;
  background-color: #0f0;
`

const Pillar = styled.div`
  width: 1px;
  height: 260px;
  background: #000;
  position: absolute;
  bottom: 25px;
  z-index: 2;
  &:hover {
    z-index: 3;
  }
`

class ChartContainer extends React.Component {
    constructor(props) {
        super(props)
        this.chartRef = React.createRef()
        this.scaleRef = React.createRef()
    }

    componentDidMount() {
        const {initChart, setEventsToStore} = this.props
        const chart = this.chartRef.current
        const {offsetWidth: scaleWidth} = this.scaleRef.current
        const sortedEvents = EVENTS
            .sort((a, b) => a.year - b.year)
        const minYear = sortedEvents[0].year
        const maxYear = sortedEvents[sortedEvents.length - 1].year
        const initialScale = scaleWidth / (maxYear - minYear)
        const eventsToDraw = sortedEvents
            .map((el) => {
                return {
                    ...el,
                    leftShift: Math.floor((el.year - minYear) * initialScale)
                }
            })
        const lastIndex = eventsToDraw.length - 1
        const lastEvent = eventsToDraw[lastIndex]
        const maxShift = Math.floor((lastEvent.year - minYear) * initialScale)
        setEventsToStore(eventsToDraw)
        initChart({
            scaleWidth,
            minYear,
            maxShift,
            currentRealMaxShift: maxShift,
            maxYear,
            initialScale,
            scale: initialScale,
        })
        chart.addEventListener('wheel', this.handleMouseWheel, {passive: false})
    }

    handleMouseWheel = (e) => {
        const {
            maxShift,
        } = this.props
        e.preventDefault()
        const deltaY = Math.floor(e.deltaY)
        const {pageX: PX} = e
        const pageX = PX - 20
        const cursorPositionIndex = pageX / maxShift
        this.handleResize(cursorPositionIndex, deltaY < 0, INCREMENT)
    }

    increaseOnce = (e) => {
        e.preventDefault()
        this.handleResize(0.5, true, INCREMENT ** 5)
    }

    decreaseOnce = (e) => {
        e.preventDefault()
        this.handleResize(0.5, false, INCREMENT ** 5)
    }

    handleResize = (cursorPositionIndex, growUp, increment) => {
        const {
            eventsToDraw,
            initialScale,
            minYear,
            maxShift,
            totalMoveToLeftSize,
            currentRealMaxShift,
            resizeChart,
            scale
        } = this.props
        let newScale
        if (growUp) {
            newScale = scale * increment
        } else {
            newScale = scale / increment
        }
        const lastIndex = eventsToDraw.length - 1
        const lastEvent = eventsToDraw[lastIndex]
        let updatedMaxShift = Math.floor((lastEvent.year - minYear) * newScale)
        const increaseSize = updatedMaxShift - currentRealMaxShift
        const updatedMoveToLeftSize = -Math.floor(increaseSize * cursorPositionIndex)
        let resultTotalMoveToLeftSize = totalMoveToLeftSize + updatedMoveToLeftSize
        let resultScale = newScale
        if (resultTotalMoveToLeftSize > 0) {
            resultTotalMoveToLeftSize = 0
        }
        if (updatedMaxShift < maxShift) {
            resultTotalMoveToLeftSize = 0
            updatedMaxShift = maxShift
            resultScale = initialScale
        }
        resizeChart({
            scale: resultScale,
            currentRealMaxShift: updatedMaxShift,
            totalMoveToLeftSize: resultTotalMoveToLeftSize
        })
    }

    drawPillars = ({eventsToDraw, maxShift}) => {
        const {minYear, scale, totalMoveToLeftSize} = this.props
        return eventsToDraw.map((el, i) => {
            const leftShift = Math.floor((el.year - minYear) * scale)
            return (
                <Pillar
                    key={el.title}
                    style={{
                        height: `${320 - (i % 5 * 50)}px`,
                        left: `${leftShift}px`
                    }}
                >
                    <EventInfoContainer
                        title={el.title}
                        dateInfo={el.dateInfo}
                        maxShift={maxShift}
                        leftShift={leftShift}
                        totalMoveToLeftSize={totalMoveToLeftSize}
                    />
                    <YearComponent
                        year={el.year}
                        maxShift={maxShift}
                        leftShift={leftShift}
                        totalMoveToLeftSize={totalMoveToLeftSize}
                    />
                </Pillar>
            )
        })
    }

    render() {
        const {
            currentRealMaxShift,
            eventsToDraw,
            maxShift,
            scale,
            totalMoveToLeftSize,
        } = this.props
        return (
            <>
                <ChartWrapper>
                    <InnerWrapper
                        ref={this.chartRef}
                        className="chart-inner__wrapper"
                    >
                        <ZoomedContainer
                            style={{
                                width: `${currentRealMaxShift}px`,
                                left: `${totalMoveToLeftSize}px`,
                            }}
                        >
                            {this.drawPillars({eventsToDraw, maxShift})}
                        </ZoomedContainer>
                        <Scale ref={this.scaleRef}/>
                    </InnerWrapper>
                </ChartWrapper>
                <ControlsContainer
                    increaseOnce={this.increaseOnce}
                    decreaseOnce={this.decreaseOnce}
                    scale={scale}
                />
            </>
        )
    }
}

const mapStateToProps = (
    {
        eventsToDraw,
        chartState: {
            scaleWidth,
            minYear,
            maxYear,
            maxShift,
            currentRealMaxShift,
            initialScale,
            scale,
            totalMoveToLeftSize,
        }
    }
) => {
    return {
        eventsToDraw,
        scaleWidth,
        minYear,
        maxYear,
        maxShift,
        currentRealMaxShift,
        initialScale,
        scale,
        totalMoveToLeftSize,
    }
}

const mapDispatchToProps = {initChart, resizeChart, setEventsToStore}

export default connect(mapStateToProps, mapDispatchToProps)(ChartContainer)