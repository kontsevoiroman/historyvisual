import React from 'react'
import styled from "styled-components";
import {connect} from "react-redux";
import {addEventToStore} from "../../store/actions";

const FormWrapper = styled.div`
  width: 100%;
`

class FormContainer extends React.Component {
    state = {
        year: '',
        title: '',
        dateInfo: '',
        details: '',
    }

    handleYearChange = (e) => this.setState({year: e.target.value})
    handleTitleChange = (e) => this.setState({title: e.target.value})
    handleDateInfoChange = (e) => this.setState({dateInfo: e.target.value})
    handleDetailsChange = (e) => this.setState({details: e.target.value})

    handleSubmit = (e) => {
        e.preventDefault();
        const { addEventToStore } = this.props
        const {dateInfo, year, title, details} = this.state
        addEventToStore({
            dateInfo,
            year,
            title,
            details,
        })
    };

    render() {
        const {year, title, dateInfo, details} = this.state
        return (
            <FormWrapper ref={this.chartRef}>
                <form
                    action=""
                    onSubmit={this.handleSubmit}
                >
                    <label htmlFor="year">
                        Год:
                        <input
                            type="number"
                            name="year"
                            id="year"
                            value={year}
                            onChange={this.handleYearChange}
                        />
                    </label>
                    <label htmlFor="title">
                        Событие:
                        <input
                            type="text"
                            name="title"
                            id="title"
                            value={title}
                            onChange={this.handleTitleChange}
                        />
                    </label>
                    <label htmlFor="dateInfo">
                        Точная дата:
                        <input
                            type="text"
                            name="dateInfo"
                            id="dateInfo"
                            value={dateInfo}
                            onChange={this.handleDateInfoChange}
                        />
                    </label>
                    <label htmlFor="details">
                        Детали:
                        <textarea
                            name="details"
                            id="details"
                            value={details}
                            onChange={this.handleDetailsChange}
                        />
                    </label>

                    <button type="submit">Сохранить</button>
                </form>
            </FormWrapper>
        )
    }
}

const mapDispatchToProps = {addEventToStore}

export default connect(null, mapDispatchToProps)(FormContainer)