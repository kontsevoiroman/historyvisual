import React from 'react';
import ChartContainer from "./chart/ChartContainer";
import './App.css';
import FormContainer from "./chart/form/FormContainer";


function App() {
    return (
        <div>
            <ChartContainer />
            <FormContainer />
        </div>
    );
}

export default App;
